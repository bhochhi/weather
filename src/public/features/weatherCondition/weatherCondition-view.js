app.config(['$routeProvider', function($routeProvider) {
	$routeProvider
	.when("/", {
		templateUrl : "features/weatherCondition/weatherCondition-partial.html",
		controller : 'WeatherConditionCtrl'
	});
}]);