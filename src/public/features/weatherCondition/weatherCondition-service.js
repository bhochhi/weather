
app.service('WeatherCondition',['$q','$resource',function($q,$resource){
	this.list =  function(searchParams){
		var deferred = $q.defer();
		var params = {};
		if(searchParams){
			params.city=searchParams[0].trim();
			params.state=searchParams[1].trim();
		}
     	var resource = $resource('/weather', {});
		resource.get(params, function (data) {
			deferred.resolve(data);
		});

		return deferred.promise;
	}
}]);