"use strict";
app.controller('WeatherConditionCtrl', ['$scope', 'WeatherCondition', function ($scope, WeatherCondition) {
    function fetchWeatherData(searchParams) {
        WeatherCondition.list(searchParams).then(function (data) {
            if (data.weatherConditions) {
                $scope.weatherConditions = data.weatherConditions;
            }
        }, function (err) {
            $scope.message = "The weather info not available for your search";
        });
    }

    $scope.submit = function () {
        var searchParams = $scope.searchText.split(',');
        fetchWeatherData(searchParams);
    };
    fetchWeatherData();
}]);