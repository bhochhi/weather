"use strict";
var express = require('express');
var middleware = require('./middleware');
var weatherApp = express();

/** registering middewares **/
function registerMiddlewares() {
    middleware.register(weatherApp);
}
registerMiddlewares();
module.exports = weatherApp;


