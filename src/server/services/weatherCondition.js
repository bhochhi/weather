/* services for weather resource */
var request = require('request');
var Weather = require('../models/weatherCondition');
var Q = require('q');
var _ = require('lodash');

function weatherForEachLocation(location){
    var deferred = Q.defer();

    var url= 'http://api.wunderground.com/api/13ad2e340e0698d0/conditions/q/'+location.state+'/'+location.city+'.json';
    function callback(error, response, body) {

        if (!error && response.statusCode == 200) {
            var rawWeather = JSON.parse(body);
            if(rawWeather.current_observation){
                deferred.resolve(new Weather(rawWeather.current_observation));
            }
            else{
                deferred.resolve({});
            }
        }
    }
    request(url,callback);
    return deferred.promise;
}

function getWeatherDetails(cityAndState){
    var self = this;
	var deferred = Q.defer();
	var promises = [];
	var locations = [];
	if(_.isEmpty(cityAndState)){ //defaults to those 4 locations if query is not defined
		locations.push({city:"Campbell",state:"CA"});
		locations.push({city:"Omaha",state:"NE"});
		locations.push({city:"Austin",state:"TX"});
		locations.push({city:"Timonium",state:"MD"});			
	}
	else{
		locations.push(cityAndState);
	}



	_.each(locations,function(loc){
		promises.push(self.weatherForEachLocation(loc));
	});

	Q.all(promises).then(function(data){
		deferred.resolve(data);
	});

	return deferred.promise;
}

exports.getWeatherDetails = getWeatherDetails;

//needed to expose for unit test.
exports.weatherForEachLocation = weatherForEachLocation;
