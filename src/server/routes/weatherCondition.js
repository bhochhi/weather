/* route for weather resource */
"use strict";

var express = require('express');
var router = express.Router();
var weatherController = require('../controllers/weatherCondition');


/** route for getting weather condition  **/
router.get('/', weatherController.getWeatherCondition);

module.exports = router;
