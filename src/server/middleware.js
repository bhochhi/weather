/**
 * Created by RBhochhibhoya on 10/8/2014.
 */
"use strict";
var express = require('express');
var path = require('path');
var bodyParser = require('body-parser');
var weatherConditionRoutes = require('./routes/weatherCondition');

var middleware = {
    register : function (weatherApp) {

        /* middleware for parse application/json */
        weatherApp.use(bodyParser.json());

        /* logging request into console */
        weatherApp.use(function logging(req, res, next) {
            console.log("=====================================================");
            console.log("info", "Request Url: ", req.url);
            console.log("info", "Request Method: ", req.method);
            console.log("info", "With parameters: ", req.query);
            console.log("  at time: ", new Date());
            next();
        });

        /* serving static pages */
        weatherApp.use(express.static(path.join(path.dirname(__dirname), 'public')));


        /* Handling weather resource, with or without querystring: '?city=city&state = state' */
        weatherApp.use('/weather', weatherConditionRoutes);

        /* Handling 404 */
        weatherApp.use(function pageNotFound(req, res) {
            res.status(404).send('404: Page not Found');
        });

        /* Handling 500*/
        weatherApp.use(function serverError(error, req, res, next) {
            res.status(505).send('500: Internal Server Error');
        });
    }
};

module.exports = middleware;