/* controller for weather resource */
"use strict";
var _ = require('lodash');

var weatherService = require('../services/weatherCondition');

function getWeatherCondition(req, res) {
	weatherService.getWeatherDetails(req.query).then(function(data){
		var results = [];
		_.each(data,function(weatherObject){
			results.push(weatherObject.serialize());
		});
		var json = {};
        json["weatherConditions"] = results;
		res.send(json);
	});
}

exports.getWeatherCondition = getWeatherCondition;
