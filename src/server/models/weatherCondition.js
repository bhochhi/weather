/* data model for weather resource */
'use strict';

var Weather = function (raw) {

	var	fullName = raw.display_location.full;
	var	city =  raw.display_location.city;
	var	state = raw.display_location.state;
	var country=  raw.display_location.country;
	var	lastUpdated=raw.observation_time;
	var	temperture=raw.temperature_string;
	var	relativeHumidity=raw.relative_humidity;
	var	weatherLike = raw.weather;
	var	wind=raw.wind_string;
	var	feelsLike=raw.feelslike_string;


	this.fullName = function(){
		return fullName;
	};

	this.city = function(){
		return city;
	};

	this.state = function(){
		return state;
	};

	this.country = function(){
		return country;
	};
	this.lastUpdated = function(){
		return lastUpdated;
	};
	this.temperture = function(){
		return temperture;
	};
	this.relativeHumidity = function(){
		return relativeHumidity;
	};

	this.weatherLike = function(){
		return weatherLike;
	};

	this.wind = function(){
		return wind;
	};

	this.feelsLike = function(){
		return feelsLike;
	};

	this.serialize = function(){
		return{
			fullName:fullName,
			city:  city,
			state: state,
			country: country,
			lastUpdated:lastUpdated,
			temperture:temperture,
			relativeHumidity:relativeHumidity,
			weatherLike:weatherLike,
			wind:wind,
			feelsLike:feelsLike
		}
	};
};


module.exports = Weather;
