
describe("Controller: weatherConditionController",function(){

    beforeEach(module('weatherApp'));
    var scope,weatherConditionCtrl,WeatherCondition,$httpBackend ;

    beforeEach(function(){
        inject(function($controller,$rootScope,_WeatherCondition_,_$httpBackend_){
            $httpBackend = _$httpBackend_;
            $httpBackend.whenGET('/weather').respond(
                { weatherConditions:[ {
                    fullName: 'Will Rogers World, OK',
                    temperature: '71 F (22 C)',
                    feelsLike: '71 F (22 C)'
                }]}
            );
            scope = $rootScope.$new();
            weatherConditionCtrl = $controller('WeatherConditionCtrl', {
                $scope: scope,
                WeatherCondition: _WeatherCondition_
            });
            scope.$apply();
            $httpBackend.flush();
        });
    });

    it("should have weatherController defined",function(){
        expect(weatherConditionCtrl).toBeDefined();
    });
    it("should have scope defined",function(){
            expect(scope).toBeDefined();
    });

    it("should have weatherCondition Information with Scope",function(){
        expect(scope.weatherConditions).toBeDefined();
    });
});