/**
 * Created by RBhochhibhoya on 10/9/2014.
 */

describe("Given a Weather Condition Service", function () {
    var WeatherConditionService, $httpBackend;
    beforeEach(module('weatherApp'));

    describe("When requesting with a valid params", function () {
        var searchParams = ['OKC', 'OK'];
        var actualData, expectedData;
        expectedData = {
            fullName: 'Will Rogers World, OK',
            temperature: '71 F (22 C)',
            feelsLike: '71 F (22 C)'
        };
        beforeEach(function () {
            inject(function (_$httpBackend_) {
                $httpBackend = _$httpBackend_;
                $httpBackend.whenGET('/weather?city=OKC&state=OK').respond(
                    {
                        fullName: 'Will Rogers World, OK',
                        temperature: '71 F (22 C)',
                        feelsLike: '71 F (22 C)'
                    }
                );


            });

            inject(function (_WeatherCondition_, $resource) {
                WeatherConditionService = _WeatherCondition_;
            });

        });


        beforeEach(function () {
            WeatherConditionService.list(searchParams)
                .then(function (data) {
                    actualData = data;
                });
            $httpBackend.flush();
        });


        it('should exist weatherConditionService', function () {
            expect(WeatherConditionService).toBeDefined();
        });


        it("should return actualData to equal expectedData ", function () {
            expect(actualData.fullName).toBe(expectedData.fullName);
            expect(actualData.temperature).toBe(expectedData.temperature);
            expect(actualData.feelsLike).toBe(expectedData.feelsLike);
        });
    });

    describe("When requesting with a undefined params", function () {
        var searchParams = undefined;
        var actualData;
        beforeEach(function () {
            inject(function (_$httpBackend_) {
                $httpBackend = _$httpBackend_;
                $httpBackend.whenGET('/weather').respond(
                    {
                        "error": {
                            "type": "querynotfound", "description": "No cities match your search query"
                        }
                    }
                );


            });

            inject(function (_WeatherCondition_, $resource) {
                WeatherConditionService = _WeatherCondition_;
            });

        });


        beforeEach(function () {
            WeatherConditionService.list(searchParams)
                .then(function (data) {
                    actualData = data;
                });
            $httpBackend.flush();
        });

        it("should return actualData with error message", function () {
            expect(actualData.error.description).toEqual("No cities match your search query");

        });
    });
});
