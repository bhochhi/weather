/**
 * Created by RBhochhibhoya on 10/8/2014.
 */

var sinon = require("sinon");
describe("Given a weather App", function () {

    describe("When WeatherApp is required", function () {
        var weatherApp, middleware, registerMiddleware;
        beforeEach(function () {
            middleware = require("./../../../src/server/middleware");
            registerMiddleware = sinon.spy(middleware, 'register');
            weatherApp = require("./../../../src/server/weatherApp");
        });
        afterEach(function(){
           middleware.register.restore();
        });
        it("should register middleware ", function () {
            expect(registerMiddleware.called).toBe(true);
        });

    });

});