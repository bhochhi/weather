/**
 * Created by RBhochhibhoya on 10/8/2014.
 */

describe("Given a weather App", function () {
    var middlewares = [];
    var app = {
        use: function (arg1, arg2) {
            if ((typeof arg1) == 'string' && (typeof arg2) == 'function') {
                middlewares.push({path: arg1, callback: arg2})
            }
            else {
                middlewares.push({callback: arg1});
            }
        }
    };

    describe("When I register middleware", function () {
        var middleware;
        beforeEach(function () {
            middleware = require("./../../../src/server/middleware");
            middleware.register(app);
        });

        it("should register body-parser for json as middleware", function () {
            expect(middlewares[0].callback.name).toEqual('jsonParser');
        });
        it("should register console logger as middleware", function () {
            expect(middlewares[1].callback.name).toEqual('logging');
        });
        it("should register middleware to serve static pages", function () {
            expect(middlewares[2].callback.name).toEqual('serveStatic');
        });
        it("should register router for weatherCondition", function () {
            expect(middlewares[3].callback.name).toEqual('router');
        });
        it("should register middleware to handle page not found error", function () {
            expect(middlewares[4].callback.name).toEqual('pageNotFound');
        });it("should register middleware to handle server error", function () {
            expect(middlewares[5].callback.name).toEqual('serverError');
        });
    });

});