/**
 * Created by RBhochhibhoya on 10/8/2014.
 */
'use strict';
var sinon = require('sinon');
var Q = require('q');
var events = require('events');
var emitter = new events.EventEmitter();
describe("Weather Condition Controller", function () {

    describe("Given a WeatherConditionController", function () {

        describe("When requiring Weather Condition Controller", function () {
            var weatherConditionController;
            weatherConditionController = require("./../../../../src/server/controllers/weatherCondition");
            it("Should have getWeatherCondition controller function", function () {
                expect(typeof weatherConditionController.getWeatherCondition).toEqual('function');
            });
        });
    });

    describe("Given a WeatherConditionController along with request object", function () {
        var weatherConditionController;

        beforeEach(function () {
            weatherConditionController = require("./../../../../src/server/controllers/weatherCondition");
        });

        describe("When querying for weather condition information with valid query", function () {
            var req, res, spyWeatherService, spyresponse;
            var weatherService = require('./../../../../src/server/services/weatherCondition');
            var mockPromise = function () {
                var rawCampbell = {
                    display_location: { full: 'Campbell, CA',
                        city: 'Campbell',
                        state: 'CA',
                        state_name: 'California',
                        country: 'US',
                        country_iso3166: 'US',
                        zip: '88888'
                    },
                    observation_time: 'Last Updated on October 8, 8:52 AM CDT',
                    weather: 'Mostly Cloudy',
                    temperature_string: '71 F (22 C)',
                    relative_humidity: '81%',
                    wind_string: 'From the SSE at 10 MPH',
                    feelslike_string: '71 F (22 C)'
                };
                var Weather = require("./../../../../src/server/models/weatherCondition");
                var weatherCampbell = new Weather(rawCampbell);
                return Q.resolve([weatherCampbell]);
            };

            beforeEach(function () {

                req = {query: {city: 'Campbell', state: 'CA'}};
                res = {send: function (json) {
                    emitter.emit('send', json);
                }};

                spyWeatherService = sinon.stub(weatherService, 'getWeatherDetails')
                    .withArgs(req.query).returns(mockPromise());
                spyresponse = sinon.spy(res, 'send');
                weatherConditionController.getWeatherCondition(req, res);


            });

            afterEach(function () {
                weatherService.getWeatherDetails.restore();
            });

            it("Should have called weather service  within controller", function (done) {
                emitter.on('send', function (data) {
                    expect(spyWeatherService.called).toBe(true);
                    done();
                });

            });

            it("Should have responded", function (done) {
                emitter.on('send', function (data) {
                    expect(spyresponse.called).toBe(true);
                    done();
                });
            });
        });
    });
});