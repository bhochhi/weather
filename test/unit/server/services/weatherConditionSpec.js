/**
 * Created by RBhochhibhoya on 10/7/2014.
 */
'use strict';
var sinon = require('sinon');
var Q = require('q');

describe("Given a Weather Condition Service", function () {
    var weatherConditionService;
    beforeEach(function () {
        weatherConditionService = require('./../../../../src/server/services/weatherCondition');
    });

    describe("Given a valid location information", function () {
        var cityAndState;
        beforeEach(function () {
            cityAndState = {city: "OKC", state: "OK"};
        });

        describe("When I request for weather Condition", function () {
            var expectedData;
            var mockPromise = function ReturnFakeOfferedProducts() {
                var raw = {
                    display_location: { full: 'Will Rogers World, OK',
                        city: 'Will Rogers World',
                        state: 'OK',
                        state_name: 'Oklahoma',
                        country: 'US',
                        country_iso3166: 'US',
                        zip: '73189',
                        magic: '5',
                        wmo: '99999',
                        latitude: '35.39305496',
                        longitude: '-97.60055542',
                        elevation: '394.00000000' },
                    observation_time: 'Last Updated on October 8, 8:52 AM CDT',
                    local_time_rfc822: 'Wed, 08 Oct 2014 09:45:59 -0500',
                    weather: 'Mostly Cloudy',
                    temperature_string: '71 F (22 C)',
                    temp_f: 71,
                    temp_c: 22,
                    relative_humidity: '81%',
                    wind_string: 'From the SSE at 10 MPH',
                    feelslike_string: '71 F (22 C)',
                    feelslike_f: '71',
                    feelslike_c: '22'
                };
                var Weather = require("./../../../../src/server/models/weatherCondition");
                var weatherObj = new Weather(raw);
                return Q.resolve(weatherObj);
            };

            function stubWunderGroundAPICall() {
                sinon.stub(weatherConditionService, 'weatherForEachLocation').returns(mockPromise());
            }

            beforeEach(function () {
                stubWunderGroundAPICall();
                expectedData = {
                    fullName: 'Will Rogers World, OK',
                    city: 'Will Rogers World',
                    state: 'OK',
                    country: 'US',
                    lastUpdated: 'Last Updated on October 8, 8:52 AM CDT',
                    temperture: '71 F (22 C)',
                    relativeHumidity: '81%',
                    weatherLike: 'Mostly Cloudy',
                    wind: 'From the SSE at 10 MPH',
                    feelsLike: '71 F (22 C)'
                };

            });
            afterEach(function () {
                weatherConditionService.weatherForEachLocation.restore()
            });

            it("should return current weather information for given cityAndState", function (done) {
                weatherConditionService.getWeatherDetails(cityAndState).then(function (data) {
                    expect(data[0].serialize()).toEqual(expectedData);
                    done();
                });

            });
        });

    });

    describe("Given an invalid location information", function () {
        var cityAndState;
        beforeEach(function () {
            cityAndState = {city: "invalid city", state: "invalid state"};
        });

        describe("When I request for weather Condition", function () {
            var expectedData;
            var mockPromise = function ReturnFakeOfferedProducts() {
                var raw = {
                    "response": {
                        "version": "0.1",
                        "termsofService": "http://www.wunderground.com/weather/api/d/terms.html",
                        "features": {
                            "conditions": 1
                        },
                        "error": {
                            "type": "querynotfound", "description": "No cities match your search query"
                        }
                    }
                };

                return Q.resolve({});
            };

            function stubWunderGroundAPICall() {
                sinon.stub(weatherConditionService, 'weatherForEachLocation').returns(mockPromise());
            }

            beforeEach(function () {
                stubWunderGroundAPICall();
                expectedData = {};

            });
            afterEach(function () {
                weatherConditionService.weatherForEachLocation.restore()
            });

            it("should return empty information", function (done) {
                weatherConditionService.getWeatherDetails(cityAndState).then(function (data) {
                    expect(data[0]).toEqual(expectedData);
                    done();
                });

            });
        });
    });

    describe("Given no location information available", function () {
        var cityAndState;
        beforeEach(function () {
            cityAndState = {};
        });

        describe("When I request for weather Condition", function () {
            var mockPromiseCambell = function () {
                var rawCampbell = {
                    display_location: { full: 'Campbell, CA',
                        city: 'Campbell',
                        state: 'CA',
                        state_name: 'California',
                        country: 'US',
                        country_iso3166: 'US',
                        zip: '88888'
                    },
                    observation_time: 'Last Updated on October 8, 8:52 AM CDT',
                    weather: 'Mostly Cloudy',
                    temperature_string: '71 F (22 C)',
                    relative_humidity: '81%',
                    wind_string: 'From the SSE at 10 MPH',
                    feelslike_string: '71 F (22 C)'
                };
                var Weather = require("./../../../../src/server/models/weatherCondition");
                var weatherCambell = new Weather(rawCampbell);
                return Q.resolve(weatherCambell);
            };
            var mockPromiseOmaha = function () {
                var rawOmaha = {
                    display_location: { full: 'Omaha, NE',
                        city: 'Omaha',
                        state: 'NE',
                        state_name: 'Nebraska',
                        country: 'US',
                        country_iso3166: 'US',
                        zip: '78787'
                    },
                    observation_time: 'Last Updated on October 8, 8:52 AM CDT',
                    weather: 'Mostly Cloudy',
                    temperature_string: '71 F (22 C)',
                    relative_humidity: '81%',
                    wind_string: 'From the SSE at 10 MPH',
                    feelslike_string: '71 F (22 C)'
                };
                var Weather = require("./../../../../src/server/models/weatherCondition");
                var weatherOmaha = new Weather(rawOmaha);
                return Q.resolve(weatherOmaha);
            };
            var mockPromiseAustin = function () {
                var rawAustin = {
                    display_location: { full: 'Austin, TX',
                        city: 'Austin',
                        state: 'TX',
                        state_name: 'Texas',
                        country: 'US',
                        country_iso3166: 'US',
                        zip: '75754'
                    },
                    observation_time: 'Last Updated on October 8, 8:52 AM CDT',
                    weather: 'Mostly Cloudy',
                    temperature_string: '71 F (22 C)',
                    relative_humidity: '81%',
                    wind_string: 'From the SSE at 10 MPH',
                    feelslike_string: '71 F (22 C)'
                };
                var Weather = require("./../../../../src/server/models/weatherCondition");
                var weatherAustin = new Weather(rawAustin);
                return Q.resolve(weatherAustin);
            };
            var mockPromiseTimonium = function () {
                var rawTimonium = {
                    display_location: { full: 'Timonium, MD',
                        city: 'Timonium',
                        state: 'MD',
                        state_name: 'Maryland',
                        country: 'US',
                        country_iso3166: 'US',
                        zip: '12121'
                    },
                    observation_time: 'Last Updated on October 8, 8:52 AM CDT',
                    weather: 'Mostly Cloudy',
                    temperature_string: '71 F (22 C)',
                    relative_humidity: '81%',
                    wind_string: 'From the SSE at 10 MPH',
                    feelslike_string: '71 F (22 C)'
                };
                var Weather = require("./../../../../src/server/models/weatherCondition");
                var weatherTimonium = new Weather(rawTimonium);
                return Q.resolve(weatherTimonium);
            };

            function stubWunderGroundAPICall() {
                sinon.stub(weatherConditionService, 'weatherForEachLocation')
                    .withArgs({city: "Campbell", state: "CA"}).returns(mockPromiseCambell())
                    .withArgs({city: "Omaha", state: "NE"}).returns(mockPromiseOmaha())
                    .withArgs({city: "Austin", state: "TX"}).returns(mockPromiseAustin())
                    .withArgs({city: "Timonium", state: "MD"}).returns(mockPromiseTimonium());
            }

            beforeEach(function () {
                stubWunderGroundAPICall();
            });
            afterEach(function () {
                weatherConditionService.weatherForEachLocation.restore()
            });

            it("should return location information for 4 default cities: Campbell,Omaha,Austin, and Timonium", function (done) {
                weatherConditionService.getWeatherDetails(cityAndState).then(function (data) {
                    expect(data[0].serialize().city).toEqual('Campbell');
                    expect(data[1].serialize().city).toEqual('Omaha');
                    expect(data[2].serialize().city).toEqual('Austin');
                    expect(data[3].serialize().city).toEqual('Timonium');
                    done();
                });

            });
        });
    });

});
