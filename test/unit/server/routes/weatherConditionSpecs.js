/**
 * Created by RBhochhibhoya on 10/8/2014.
 */
var _ = require('lodash');
describe("Given weather condition routes",function(){
    var weatherConditionRoutes = require("./../../../../src/server/routes/weatherCondition");
    var routes;
    beforeEach(function(){
        routes = _.pluck(weatherConditionRoutes.stack,'route');
    });

    it("should have route for weather condition with Get method",function(){
        expect(_.find(routes,{path: '/', methods: { get: true }})).toBeDefined();
    });
});