describe('Given a raw data from  wunderground.com and WeatherCondition data Model is available', function () {
	var rawData,WeatherCondition,weatherObject;
	beforeEach(function () {
		WeatherCondition = require("../../../../src/server/models/weatherCondition");
		rawData= {
			"response": {
				"version":"0.1",
				"termsofService":"http://www.wunderground.com/weather/api/d/terms.html",
				"features": {
					"conditions": 1
				}
			},
			"current_observation": {
				"display_location": {
					"full":"Omaha, NE",
					"city":"Omaha",
					"state":"NE",
					"state_name":"Nebraska",
					"country":"US"
				},
				"observation_time":"Last Updated on September 24, 8:10 PM CDT",
				"weather":"Scattered Clouds",
				"temperature_string":"69.1 F (20.6 C)",
				"relative_humidity":"73%",
				"wind_string":"Calm",
				"feelslike_string":"69.1 F (20.6 C)",
			}
		};

		expectedSerializedData = { 
			fullName : 'Omaha, NE', 
			city : 'Omaha', 
			state : 'NE', 
			country : 'US', 
			lastUpdated : 'Last Updated on September 24, 8:10 PM CDT', 
			temperture : '69.1 F (20.6 C)', 
			relativeHumidity : '73%', 
			weatherLike : 'Scattered Clouds', 
			wind : 'Calm', 
			feelsLike : '69.1 F (20.6 C)' 
		}
	});
	describe('When an weather object is create',function(){
		beforeEach(function () {
			weatherObject = new WeatherCondition(rawData.current_observation);
		});

		it('should have fullName from rawData ', function () {
			expect(weatherObject.fullName()).toEqual(rawData.current_observation.display_location.full);
		});
		it('should have city from rawData ', function () {
			expect(weatherObject.city()).toEqual(rawData.current_observation.display_location.city);
		});
		it('should have state from rawData ', function () {
			expect(weatherObject.state()).toEqual(rawData.current_observation.display_location.state);
		});
		it('should have country from rawData ', function () {
			expect(weatherObject.country()).toEqual(rawData.current_observation.display_location.country);
		});
		it('should have lastUpdated from rawData ', function () {
			expect(weatherObject.lastUpdated()).toEqual(rawData.current_observation.observation_time);
		});
		it('should have temperture from rawData ', function () {
			expect(weatherObject.temperture()).toEqual(rawData.current_observation.temperature_string);
		});
		it('should have relativeHumidity from rawData ', function () {
			expect(weatherObject.relativeHumidity()).toEqual(rawData.current_observation.relative_humidity);
		});
		it('should have weatherLike from rawData ', function () {
			expect(weatherObject.weatherLike()).toEqual(rawData.current_observation.weather);
		});
		it('should have wind from rawData ', function () {
			expect(weatherObject.wind()).toEqual(rawData.current_observation.wind_string);
		});
		it('should have feelsLike from rawData ', function () {
			expect(weatherObject.feelsLike()).toEqual(rawData.current_observation.feelslike_string);
		});
		it('should have serialized data',function(){
			expect(weatherObject.serialize()).toEqual(expectedSerializedData);
		});

	});
});